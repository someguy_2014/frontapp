
require 'sinatra'
require 'net/http'
require 'uri'
require 'json'

set :public_folder, 'public'

uri = URI.parse('http://localhost:3000/fibonacci')

get '/fibonacci' do
  if params[:n]
    uri.query = URI.encode_www_form(params)
    response  = Net::HTTP.get_response(uri)
    table     = JSON.parse(response.body)
  end

  erb :fibonacci, locals: { size: params[:n], table: table, saved: false} , layout: :application
end

post '/fibonacci' do
  table = []
  saved = false
  if params[:n]
    response = Net::HTTP.post_form(uri, params)
    table    = JSON.parse(response.body)
    saved    = response.is_a?(Net::HTTPSuccess)
  end

  erb :fibonacci, locals: { size: params[:n], table: table, saved: saved }, layout: :application
end
